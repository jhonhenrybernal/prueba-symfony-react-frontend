import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import CheckIcon from '@mui/icons-material/Check';
export const Task = (props) => {
    return (
      <div
        className="task"
        style={{ backgroundColor: props.completed ? "green" : "white" }}
      >
        <h1>{props.taskName}</h1>
        <IconButton aria-label="delete" onClick={() => props.deleteTask(props.id)}>
            <DeleteIcon />
        </IconButton>
        <IconButton color="primary"  onClick={() => props.completeTask(props.id)}>
            <CheckIcon />
        </IconButton>
      </div>
    );
  };