import "./App.css";
import { useState } from "react";
import { Task } from "./Task";
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import Divider from '@mui/material/Divider';

/**
 * This is a React component that allows users to add, delete, and mark tasks as completed in a to-do
 * list.
 * @returns The `App` component is returning a div with two child components:
 */
function App() {
  const [todoList, setTodoList] = useState([]);
  const [newTask, setNewTask] = useState("");

 /**
  * The handleChange function sets the value of a new task based on the input event.
  * @param event - The event parameter is an object that contains information about the event that
  * triggered the handleChange function. In this case, it is likely an event that is triggered when the
  * user types something into an input field. The event object contains properties such as target,
  * which refers to the element that triggered the event, and
  */
  const handleChange = (event) => {
    setNewTask(event.target.value);
  };

  /**
   * This function adds a new task to a todo list if the task name is not empty.
   */
  const addTask = () => {
    const task = {
      id: todoList.length === 0 ? 1 : todoList[todoList.length - 1].id + 1,
      taskName: newTask,
      completed: false,
    };
    setTodoList(task.taskName !== "" ? [...todoList, task] : todoList);
  };

 /**
  * This function deletes a task from a todo list based on its ID.
  * @param id - The `id` parameter is a unique identifier for a task in a todo list. The `deleteTask`
  * function takes this `id` as an argument and removes the task with that `id` from the `todoList`
  * array using the `filter` method.
  */
  const deleteTask = (id) => {
    setTodoList(todoList.filter((task) => task.id !== id));
  };

 /**
  * The function `completeTask` updates the `completed` property of a task with a given `id` to `true`
  * in a todo list.
  * @param id - The `id` parameter is a unique identifier for a task in a todo list. It is used in the
  * `completeTask` function to find the task with the matching `id` and update its `completed` property
  * to `true`.
  */
  const completeTask = (id) => {
    setTodoList(
      todoList.map((task) => {
        if (task.id === id) {
          return { ...task, completed: true };
        } else {
          return task;
        }
      })
    );
  };

  return (
    <div className="App">
      <div className="addTask">
        <Paper
        component="form"
        sx={{ p: '2px 4px', display: 'flex', alignItems: 'center', width: 400 }}
      >
        <InputBase
          sx={{ ml: 1, flex: 1 }}
          placeholder="Añadir nueva tarea"
          inputProps={{ 'aria-label': 'Añadir nueva tarea' }}
          onChange={handleChange}
        />
        <Divider sx={{ height: 28, m: 0.5 }} orientation="vertical" />
        <Button variant="contained" onClick={addTask}>Añadir Nueva tarea</Button>
      </Paper>
      </div>
      {/* <div className="addTask">
        <TextField id="filled-basic" label="Tarea" variant="filled"  onChange={handleChange}/>
        <Button variant="contained" onClick={addTask}>Añadir Nueva tarea</Button>
      </div> */}
      <div className="list">
        {todoList.map((task) => {
          return (
            <Task
              taskName={task.taskName}
              id={task.id}
              completed={task.completed}
              deleteTask={deleteTask}
              completeTask={completeTask}
            />
          );
        })}
      </div>
    </div>
  );
}

export default App;