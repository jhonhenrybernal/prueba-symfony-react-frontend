# Requerimiento

1.	Crear un proyecto ReactJS con las dependencias estándar
2.	Implementar una librería para el UI
3.	Implementar un TodoList
4.	Agregar la opción de crear tareas
5.	Agregar la opción de marcar tareas como finalizadas
6.	Agregar la opción de eliminar tareas
7.	Utilizar hooks de React incluyendo el useContext
8.	Hacer uso de buenas prácticas desarrollo

# Iniciar proyecto

### `npm install`
El proyecto uniciara la instalación de depedencias.

### `npm start`

Ejecuta la aplicación en el modo de desarrollo.\
Abra [http://localhost:3000](http://localhost:3000) para verlo en su navegador.

La página se volverá a cargar cuando realice cambios.\
También puede ver errores de pelusa en la consola.
